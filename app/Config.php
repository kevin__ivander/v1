<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config_url';
    protected $primaryKey = 'id';
    protected $fillable = ['url'];
    public $timestamps = false;
}
