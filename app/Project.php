<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $primaryKey = 'id';
    protected $fillable = ['name',
    						'client',
    						'user'];
    public $timestamps = false;
}
