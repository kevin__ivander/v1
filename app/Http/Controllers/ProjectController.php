<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Project;

use App\Path;

use Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');        
    }
    
    public function index()
    {
        $id_user=Auth::User()->id;
        $datas = Project::where('id_user',$id_user)->orderBy('id','DESC')->paginate(10);
        return view('projects.project')->with('datas',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.addproject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:project|max:255',
            'client'=>'required',
            'user'=>'required', 
        ]);

        if ($validator->fails()) {
            return redirect()->route('project.index')
                        ->withErrors($validator)
                        ->withInput();
        }

        $project = new project;
        $project->id_user=Auth::User()->id;
        $project->name=$request->name;
        $project->client=$request->client;
        $project->user=$request->user;
        $project->save();
        
        return redirect()->route('project.index')->with('alert-success','Data Has been Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('/path/'.$id)->with('id',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::where('id', $id)->first();
        return view('projects.editproject',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
        'name'=>'required',
        'client'=>'required',
        'user'=>'required',    
        ]);

        $projects = Project::where('id','!=', $id)->get();
        if($projects!=null)
        {
            foreach ($projects as $proj) {
                if($request->name==$proj->name)
                {
                    return redirect()->route('project.index')->with('alert-fail','Data Can Not be Same');
                }
            }
        }

        $project = Project::where('id', $id)->first();
        $project->name=$request->name;
        $project->client=$request->client;
        $project->user=$request->user;
        $project->save();
        
        return redirect()->route('project.index')->with('alert-success','Data Has been Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Project::find($id);
        $hapus->delete();

        return redirect()->route('project.index')->with('alert-success','Data Has been Deleted!');
    }
}
