<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\Http\Requests;

use App\Project;

use App\Path;

use App\User;

use JWTAuth;

use Tymon\JWTAuth\Exception\JWTException;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function makeApi(Request $request, $project, $endpoint)
    {
        $result = $request->input('result');

        $id_project = Project::select('id')->where('name',$project)->first();
        
        if($result=="ok")
        {
            $data = Path::where('id_project',$id_project->id)->where('endpoint',$endpoint)->first();
            $value = json_decode($data->result_ok);
        }
        elseif($result=="fail")
        { 
            $data = Path::where('id_project',$id_project->id)->where('endpoint',$endpoint)->first();
            $value = json_decode($data->result_fail);
        }
        elseif($result==""||$id_project==null||$data==null)
        {
            $value = ['status' => false,
                    'status_code' => 201,
                    'message' => "Data not found",
                    'respon' => null
            ];
        }
        
        return response()->json($value);

    }

    public function registerapi(Request $request)
    {        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        User::create($input);
        return response()->json(['result'=>true]);
    }
    
    public function loginapi(Request $request)
    {
        $input = $request->only('email','password');
        if(!$token = JWTAuth::attempt($input)){
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        } else {
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $token;
        }
        
        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'token' => $value
        ];
        if (!$token = JWTAuth::attempt($input)) {
            return response()->json($res);
        }
            return response()->json($res);
    }
    
    public function get_user_details(Request $request)
    {
        $input = $request->all();
        $user = JWTAuth::toUser($input['token']);
        return response()->json(['result' => $user]);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
