<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atribute extends Model
{
    protected $table = 'atributes';
    protected $primaryKey = 'id';
    protected $fillable = ['key',
    						'value',
    						'type',
    						'id_path'];
    public $timestamps = false;
}
