@extends('layouts.template_add_project')

@section('content')
<nav id="menu" class="menu">
    <ul id="tiny">
        <li class="active"><a href="{{ url('/homes') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
        </li>
        <li><a href="{{ url('/home') }}"><i class="fa fa-cubes" aria-hidden="true"></i> Project</a>
        </li>
        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hideen="true"></i> Logout</a>
        </li>    
    </ul>
</nav>

<div class="box box-border pull-left">
    <div class="light-wrapper">
        <div class="container inner">
            <div class="wide-bannercontainer revolution">
                <div class="wide-banner">
                    <ul>
                        <li data-transition="fade"> <img src="{{config('app.url')}}public/style/images/art/slider-bg1.jpg" alt="" />
                            <div class="caption sfl white-bg big text-center" data-x="center" data-y="175" data-speed="500" data-start="100" data-easing="easeOutExpo">Welcome To FAKE API</div>   
                        </li>

                        <li data-transition="fade"> <img src="{{config('app.url')}}public/style/images/art/slider-bg2.jpg" alt="" />
                            <div class="caption sfl white-bg big text-center" data-x="center" data-y="175" data-speed="500" data-start="100" data-easing="easeOutExpo">A Free API Prototype maker</div>  
                        </li>

                        <li data-transition="fade"> <img src="{{config('app.url')}}public/style/images/art/slider-pages.png" alt="" />
                            <div class="caption sfl white-bg big text-center" data-x="center" data-y="175" data-speed="500" data-start="100" data-easing="easeOutExpo">Easier, better, and faster</div>
                        </li>

                        <li data-transition="fade"> <img src="{{config('app.url')}}public/style/images/art/slider-bg3.jpg" alt="" />
                            <div class="caption sfl white-bg big text-center" data-x="center" data-y="175" data-speed="500" data-start="100" data-easing="easeOutExpo">Stressed Out ? Get some coffe</div> 
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
