<html class=" js no-touch" lang="en">
<head>
	<meta charset="utf-8">
	@yield('title')
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script src="/s/dist/lib/landing/modernizr.custom.js"></script>
	
	<link href="{{config('app.url')}}public/css/animate.css" rel="stylesheet">
	
	<link href="{{config('app.url')}}public/css/bootstrap.min.css" rel="stylesheet">
	
	<link href="http://fonts.googleapis.com/css?family=Nunito:400,300,700" rel="stylesheet" type="text/css">
	<link href="{{config('app.url')}}public/css/marketing.css" rel="stylesheet">
	<link href="{{config('app.url')}}public/css/queries.css" rel="stylesheet">
	<link href="{{config('app.url')}}public/css/style2.css" rel="stylesheet">
</head>

<body>
	<header>
		<section class="hero">
			<div class="texture-overlay"></div>
			<div class="container">
				<div class="row nav-wrapper">
					<div class="col-md-6 col-sm-6 col-xs-6 text-left">
						<a class="welcome-logo" href="{{ url('/') }}"><img src="img/logo.png"> Fake API</a>
					</div>
				</div>
				@yield('content')
			</div>
		</section>
	</header>

<script src="{{config('app.url')}}public/js/toucheffects-min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{config('app.url')}}public/js/waypoints.min.js"></script>
<script src="{{config('app.url')}}public/js/bootstrap.min.js"></script>
<script src="{{config('app.url')}}public/js/scripts.js"></script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-10658710-3' ]);
	_gaq.push([ '_trackPageview' ]);

	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl'
				: 'http://www')
				+ '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
<script type="text/javascript">

        var urlBase = "<?php echo Request::root(); ?>"

        $(function(){

              function requestData1() {
                $.ajax({
                    url: urlBase + '/session/ajaxData1',
                    success: function(point) {

                        setTimeout(requestData1, 500);    
                    },
                    cache: false
                });
              }

              requestData1();

        });
    </script>
<script type="text/javascript" charset="utf-8" async="" src="https://platform.twitter.com/js/tweet.08991409fe8c7862c0aa5cc77e44569a.js"></script>

<iframe id="rufous-sandbox" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" style="position: absolute; visibility: hidden; display: none; width: 0px; height: 0px; padding: 0px; border: none;" title="Twitter analytics iframe"></iframe>
</body>

</html>
