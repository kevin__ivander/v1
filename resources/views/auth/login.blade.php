@extends('layouts.template')

@section('title')
<title>Login</title>
@endsection

@section('content')
</br></br></br></br></br></br></br></br></br></br></br></br>
<div class="row hero-content">
    <div class="col-md-12">
        
        <div class="header">
            <div>Fake<span>API</span></div>
        </div>
        <br>
        <form role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
            <div class="login">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" placeholder="email" name="email" value="{{ old('email') }}" required=""><br>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" placeholder="password" name="password" required=""><br>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="submit" value="Login">
                </div>    
            </div>
        </form>
    </div>
</div>
@endsection

