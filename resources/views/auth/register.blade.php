@extends('layouts.template')

@section('title')
<title>Register</title>
@endsection

@section('content')
</div></br></br></br></br></br></br></br></br></br></br>
<div class="row hero-content">
    <div class="col-md-12">
        <div class="header">
            </br></br>
            <div>Fake<span>API</span></div>
        </div>
        <br>
        <div class="register">
            <form class="newsletter" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" name="name" value="{{ old('name') }}" placeholder="Username" required="">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" required="">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" name="password" placeholder="Password" required="">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" name="password_confirmation" placeholder="password_confirmation" required="">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">   
                    <input type="submit" value="Register">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

